This is my version of the NSLogger facility (https://github.com/fpillet/NSLogger.git).

The main difference is a backport to Mac OS X 10.6 , which depends on my UglySnowLing framework:
http://github.com/RJVB/UglySnowLing
